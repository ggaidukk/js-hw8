1. Це особливий інтерфейс, який показує, як HTML документи читаються браузером. Браузер читає HTML документ, а потім представляє дані у формі дерева і визначає структуру доступу до цього дерева.
2. innerText - показує весь текстовий вміст, який не належить до синтаксису HTML. Тобто будь-який текст, укладений між відкриваючими і закриваючими тегами елемента, буде записано в innerText.
innerHTML - покаже текстову інформацію рівно по одному елементу. У виведення потрапить і текст, і розмітка HTML-документа, яка може бути укладена між тегами основного елемента, що відкривають і закривають.
3. getElementById()
getElementsByName()
getElementsByTagName()
getElementsByClassName()
querySelectorAll()
querySelector() - найкращий 