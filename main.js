const allParagraphs = document.querySelectorAll("p");
allParagraphs.forEach((paragraph) => paragraph.style.backgroundColor = "#ff0000");
const optionsList = document.getElementById("optionsList");
console.log(optionsList);
const parentOptionList = optionsList.parentElement;
console.log(parentOptionList);

if (optionsList.hasChildNodes()) {
  const optionsListChildNodes = optionsList.childNodes;
  optionsListChildNodes.forEach((child) =>
    console.log(`name - ${child.nodeName}, type - ${child.nodeType}`)
  );
}

const p = document.getElementById("testParagraph");
p.className = "testParagraph";
p.innerHTML = "This is a paragraph";

const e = document.querySelector(".main-header");
for (const child of e.children) {
  console.log(child);
  child.classList.add("nav-item");
}
const getAllChildren = (e) => {
  if (e.children.length === 0) return [e];
  const allChildElements = [];
  for (let i = 0; i < e.children.length; i++) {
    let children = getAllChildren(e.children[i]);
    if (children) allChildElements.push(...children);
  }
  allChildElements.push(e);
  return allChildElements;
};
const elementChildrens = getAllChildren(e);
console.log(elementChildrens);
elementChildrens.forEach((e) => e.classList.add("nav-item"));

const sectionTitle = document.querySelectorAll(".section-title");
sectionTitle.forEach((element) => element.classList.remove("section-title"));
console.log(sectionTitle);
